let btn_ex = document.querySelector('.delete')
let btn_add = document.querySelector('#add-book button')
let book = document.getElementById('name')
let author = document.getElementById('author')
let listao = document.querySelector('ul')

function postJSON(infobook) {
    fetch('https://treinamento-api.herokuapp.com/books', {
        method: "post",
        body: JSON.stringify(infobook),
        headers: {
            "Content-Type": "application/json"
        }
    })
}

function removeByID(id) {
    fetch(`https://treinamento-api.herokuapp.com/books/${id}`, {
        method: "delete",
        headers: {
            "Content-Type": "application/json"
        }
    })
}

const ParseRequestToJSON = (requestResult) => {
    return requestResult.json()
}

fetch('https://treinamento-api.herokuapp.com/')
    .then(ParseRequestToJSON)
    .then(books => {
        for (let i = 0; i < books.length; i++) {
            let livro = document.createElement('li');
            livro.innerHTML = `<span class="name">${books[i].name}, do autor ${books[i].author}<br>
            Adicionado em: ${books[i].created_at}.<br>Ultima Atualização em: ${books[i].updated_at}<br> ID: </span><span class = "id">${books[i].id}</span>\n<span class="delete" id =${books[i].id}>excluir</span>`;
            listao.appendChild(livro);
        }
    })


btn_add.addEventListener('click', (e) => {
    let nomelivro = book.value
    let autorlivro = author.value
    let infobook = {
        "name": `${nomelivro}`,
        "author": `${autorlivro}`
    }
    postJSON(infobook)
})


//obsoleto para o momento
listao.addEventListener('click', e => {
    if (e.target.className == 'delete') {
        removeByID(e.target.id)
        e.target.parentElement.parentElement.removeChild(e.target.parentElement);
    }
})